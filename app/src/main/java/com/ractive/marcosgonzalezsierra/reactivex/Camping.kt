package com.ractive.marcosgonzalezsierra.reactivex

data class Camping(val nombre: String = "", val municipio: String = "", val telefono: String = "") {}