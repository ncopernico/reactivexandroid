package com.ractive.marcosgonzalezsierra.reactivex

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.SearchView
import io.reactivex.*
import io.reactivex.Observable
import java.util.*
import java.util.Observer
import com.jakewharton.rxbinding2.*
import com.jakewharton.rxbinding2.widget.RxSearchView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class MainActivity : AppCompatActivity() {

    private val spotifyPresent = SpotifyClient()

    private val bag = CompositeDisposable()

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)

            val searchbar = findViewById<SearchView>(R.id.search_bar)

            spotifyPresent.context = this


                RxSearchView.queryTextChanges(searchbar)
                        .flatMap { textToQuery ->
                            spotifyPresent.campings(textToQuery.toString())
                                    .onErrorReturnItem(mutableListOf<Camping>())
                        }
                        .map { campings ->
                             campings.map { CampingRenderable(nombre = it.nombre) }
                        }
                        .subscribeBy(
                                onNext = {
                                    println("--->" + it)

                                }
                        )
                        .addTo(bag)

            }

        override fun onDestroy() {
            bag.clear()
        super.onDestroy()
    }
}
