package com.ractive.marcosgonzalezsierra.reactivex

import android.content.Context
import android.util.Base64
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import  io.reactivex.*
import org.json.JSONObject
import java.nio.charset.StandardCharsets
import com.android.volley.VolleyError
import org.json.JSONArray
import com.android.volley.toolbox.JsonArrayRequest




class SpotifyClient {

    var context: Context? = null
    var queue: RequestQueue? = null

    fun campings ( text: String ): Observable<MutableList<Camping>> {

        return Observable.create { observer ->

            queue = Volley.newRequestQueue(context)

            val PLACES_URL = "http://nexo.carm.es/nexo/archivos/recursos/opendata/json/Campings.json"

            var campings = mutableListOf<Camping>()

            //Prepare the Request
            val request = JsonArrayRequest(
                    Request.Method.GET, //GET or POST
                    PLACES_URL, //URL
                    null, //Parameters
                    Response.Listener { responsePlaces ->

                        val strResp = responsePlaces.toString()
                        val jsonArray: JSONArray = JSONArray(strResp)

                        for (i in 0 until jsonArray.length()) {
                            val jsonInner: JSONObject = jsonArray.getJSONObject(i)
                            val camping = Camping(nombre = jsonInner.getString("Nombre"), municipio = jsonInner.getString("Municipio"), telefono = jsonInner.getString("Teléfono"))

                            if (camping.nombre.contains(text.toUpperCase())) { //Simulo que el servidor soporta filtrado por nombre
                                campings.add(camping)
                            }
                        }

                        observer.onNext(campings)
                        observer.onComplete()

                    }, Response.ErrorListener { error ->
                observer.onError(error)
            })

            queue?.add(request)
        }
    }
}